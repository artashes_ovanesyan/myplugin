package ru.artafon.myPlugin.jira.webwork.service;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;

import javax.inject.Named;

import static webwork.action.Action.SUCCESS;

@Named
public class PluginSettingService {
    public static final String PLUGIN_STORAGE_KEY = "ru.artafon.myPlugin.myPlugin.";
    private final PluginSettings pluginSettings;
    public String parameter1;
    public String parameter2;

    public String getParameter1() {
        return parameter1;
    }

    public void setParameter1(String parameter1) {
        this.parameter1 = parameter1;
    }

    public String getParameter2() {
        return parameter2;
    }

    public void setParameter2(String parameter2) {
        this.parameter2 = parameter2;
    }

    public PluginSettingService(@ComponentImport PluginSettingsFactory pluginSettingsFactory){
        this.pluginSettings = pluginSettingsFactory.createGlobalSettings();
        this.parameter1 = pluginSettings.get(PLUGIN_STORAGE_KEY + "parameter1") == null ? "" : pluginSettings.get(PLUGIN_STORAGE_KEY + "parameter1").toString();
        this.parameter2 = pluginSettings.get(PLUGIN_STORAGE_KEY + "parameter2") == null ? "" : pluginSettings.get(PLUGIN_STORAGE_KEY + "parameter2").toString();
    }

    public String doExecute() throws Exception {
        return SUCCESS;
    }

    public String doSave() {
        this.pluginSettings.put(PLUGIN_STORAGE_KEY + "parameter1", this.parameter1);
        this.pluginSettings.put(PLUGIN_STORAGE_KEY + "parameter2", this.parameter2);
        return SUCCESS;
    }

    public String doClear() {
        this.pluginSettings.remove(PLUGIN_STORAGE_KEY + "parameter1");
        this.pluginSettings.remove(PLUGIN_STORAGE_KEY + "parameter2");
        this.parameter1 = pluginSettings.get(PLUGIN_STORAGE_KEY + "parameter1") == null ? "" : pluginSettings.get(PLUGIN_STORAGE_KEY + "parameter1").toString();
        this.parameter2 = pluginSettings.get(PLUGIN_STORAGE_KEY + "parameter2") == null ? "" : pluginSettings.get(PLUGIN_STORAGE_KEY + "parameter2").toString();
        return SUCCESS;
    }
}
