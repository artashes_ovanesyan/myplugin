package ru.artafon.myPlugin.jira.webwork;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import ru.artafon.myPlugin.jira.webwork.service.PluginSettingService;

import javax.inject.Inject;
import java.util.regex.Pattern;

public class NewFormAction extends JiraWebActionSupport
{
    private static final Logger log = LoggerFactory.getLogger(NewFormAction.class);
    private PluginSettingService pluginSettingService;
    private String parameter1;
    private String parameter2;

    public String getParameter1() {
        return pluginSettingService.getParameter1();
    }

    public void setParameter1(String parameter1) {
        pluginSettingService.setParameter1(parameter1);
    }

    public String getParameter2() {
        return pluginSettingService.getParameter2();
    }

    public void setParameter2(String parameter2) {
        pluginSettingService.setParameter2(parameter2);
    }

    @Inject
    public NewFormAction(PluginSettingService pluginSettingService){
        this.pluginSettingService = pluginSettingService;
    }

    @Override
    public String doExecute() throws Exception {
        return SUCCESS;
    }

    public String doSave(){
        if (!Pattern.matches("^[0-9]+$", getParameter2()) || Integer.parseInt(getParameter2()) == 0){
            return ERROR;
        }
        return pluginSettingService.doSave();
    }

    public String doClear(){
        return pluginSettingService.doClear();
    }
}